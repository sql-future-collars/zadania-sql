/* 1. Za�� baz� o nazwie: School_Coding. */
CREATE DATABASE "School_Coding";

/* 2. Za�� tabel� o nazwie: Mentors. */

CREATE TABLE Mentors (
	ID INTEGER UNIQUE NOT NULL,
	Imi� VARCHAR(255) NOT NULL,
	Nazwisko VARCHAR(255) NOT NULL,
	Specjalizacja VARCHAR(255) NOT NULL,
	Data_zatrudnienia DATE NOT NULL, 
	Data_zwolnienia DATE DEFAULT ' N/A'
); 


/* 3. Utw�rz 6 kolumn, a tak�e dobierz i ustaw dla ka�dej CONSTRAINTS:
ID
Imi� 
Nazwisko
Specjalizacja
Data zatrudnienia
Data zwolnienia */

/* 4. Wprowad� dane (10 pozycji od 1 do 10). Ka�da pozycja ma mie� wype�nione wszystkie dane zgodnie z wymaganiem).  pozostawi�am default na 2 nie zwolnionych */

INSERT INTO Mentors (ID, Imi�, Nazwisko, Specjalizacja, Data_zatrudnienia, Data_zwolnienia)
VALUES  (1, 'Marek', 'Nowak', 'IT', '05.02.2020' , ' 03.10.2020' ) ,
		(2, 'Maria', 'Kowalska' , 'HR', '10.10.2020' , ' 01.04.2021 ' ) ,
		(3, 'Tomasz', 'Skalski' , 'Sales', '02.11.2019', '05.12.2020' ) ,
		(4, 'Marta', 'Bara�ska' , 'IT', '05.05.2018', ' 01.03.2021 ' ) ,
		(5, 'Andrzej', 'Wieczorek' , 'Finance', '06.07.2015', ' ' ) ,
		(6, 'Anna' , 'Kowalczyk' , 'HR', '10.12.2015', ' 01.03.2021 ' ) ,
		(7, 'Ma�gorzata' , 'St�pie�' , 'Finance', '01.01.2010' , '10.03.2021 ' ) ,
		(8, 'Tomasz' , 'Jaworski' , 'IT', '05.05.2020', ' 01.03.2021 ' ) ,
		(9, 'Marcin' , 'Lisowski' , 'Sales', '10.03.2017' , ' ') ,
		(10, 'Aneta' , 'Mroz' , 'HR', '01.04.2016' , ' 03.03.2021' ) ;



/* 5. Sprawd�, czy wszystkie dane wy�wietlaj� si� prawid�owo. */
Select * 
FROM Mentors ;

/* 6. Zmie� Nazwisko Mentora w pozycji nr 5. */

UPDATE Mentors
SET Nazwisko = 'Tyczy�ski'
WHERE id = 5 ; 

/* 7. Sprawd�, czy dane zosta�y zmienione, wy�wietlaj�c tylko t� pozycj�. */
Select * 
FROM Mentors 
WHERE id=5;

/* 8. Zmie� stanowisko mentora w pozycji 9. */

UPDATE Mentors
SET Specjalizacja = 'Trainee'
WHERE id = 9 ; 

/* 9. Sprawd�, czy dane zosta�y zmienione, wy�wietlaj�c tylko t� pozycj�. */
Select * 
FROM Mentors 
WHERE id=9;

/* 10. Dodaj kolumn� o nazwie Wynagrodzenie, a tak�e dobierz i ustaw CONSTRAINTS. */

ALTER TABLE Mentors ADD Wynagrodzenie FLOAT ; 

/* 11. Uzupe�nij kolumn� warto�ci� 1000 dla pozycji 1,2,3. */

UPDATE Mentors
SET Wynagrodzenie = 1000
WHERE id = 1 OR id=2 or id=3; 

/* 12. Wy�wietl pozycj� 1,2,3 i sprawd�, czy dane s� prawid�owe. */

SELECT * 
FROM Mentors
WHERE id =1 or id= 2 or id = 3 ; 